<?php namespace Luminati;

use HttpClient\Request;

class Zone {
    const API_ENDPOINT = 'https://luminati.io/api';

    protected $service;
    protected $name;

    /**
     * Пароль зоны
     * @var string
     */
    protected $password;

    public function __construct(Luminati $service, $name, $password)
    {
        $this->service = $service;
        $this->name = $name;
        $this->password = $password;
    }

    public function getStatistics()
    {
        $response =  $this->request('get_zone_bw', [
            'zone'      => $this->name,
            'details'   => 1
        ]);

        return $response->getBody();
    }

    public function getRouteVips()
    {
        $response =  $this->request('get_route_vips');
        return $response->getBody();
    }

    public function refreshIps($ips = [])
    {
        $requestData = [];
        $this->addAuthDataToRequest($requestData);
        $this->addZoneDataToRequest($requestData);
        $requestData['ips'] = $ips;

        $request = new Request(sprintf('%s/%s', static::API_ENDPOINT, 'refresh'));
        $request->setHeader('Content-Type', 'application/json');
        $request->setData(json_encode($requestData));

        return json_decode((new \HttpClient\Client())->sendRequest($request)->getBody());
    }

    /**
     * @param $requestData
     */
    private function addAuthDataToRequest(&$requestData)
    {
        $requestData['email'] = $this->service->getEmail();
        $requestData['password'] = $this->service->getPassword();
    }

    /**
     * @param $requestData
     */
    private function addZoneDataToRequest(&$requestData)
    {
        $requestData['customer'] = $this->service->getCustomer();
        $requestData['zone'] = $this->name;
    }

    /**
     * @param $method
     * @param array $data
     * @return \HttpClient\Response
     */
    private function request($method, $data = [])
    {
        $uri = sprintf('%s/%s?%s', static::API_ENDPOINT, $method, http_build_query($data));

        $request = new Request($uri);
        $request->setHeader('X-Hola-Auth', $this->getXHolaAuthString());

        return (new \HttpClient\Client())->sendRequest($request);
    }

    /**
     * @return string
     */
    private function getXHolaAuthString()
    {
        return sprintf('lum-customer-%s-zone-%s-key-%s', $this->service->getCustomer(), $this->name, $this->password);
    }
}