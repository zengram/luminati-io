<?php namespace Luminati;

class Luminati {
    private $customer;
    private $email;
    private $password;

    /**
     * Luminati constructor.
     * @param $customer
     * @param $email
     * @param $password
     */
    public function __construct($customer, $email = null, $password = null)
    {
        $this->customer = $customer;
        $this->email    = $email;
        $this->password = $password;
    }

    /**
     * Получение зоны по ее имени
     *
     * @param string $name имя зоны
     * @param string $zonePassword пароль для использования зоны
     * @return Zone
     */
    public function zone($name, $zonePassword)
    {
        return new Zone($this, $name, $zonePassword);
    }

    /**
     * @return string|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return null
     */
    public function getEmail()
    {
        return $this->email;
    }
}